package main

import (
	"encoding/json"
	"encoding/xml"
	"flag"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"sort"
	"strconv"
	"strings"

	"golang.org/x/text/encoding/charmap"
	"gopkg.in/yaml.v3"
)

type Config struct {
	Input  string `yaml:"input-file"`
	Output string `yaml:"output-file"`
}

type ValuteInfoDemo struct {
	NumCode  int64  `xml:"NumCode"`
	CharCode string `xml:"CharCode"`
	Value    string `xml:"Value"`
}

type ValuteInfo struct {
	NumCode  int64   `xml:"NumCode" json:"num_code"`
	CharCode string  `xml:"CharCode" json:"char_code"`
	Value    float64 `xml:"Value" json:"value"`
}

type ValCurs struct {
	Valute []ValuteInfo `xml:"Valute"`
}

func (valute *ValuteInfo) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	valuteDemo := ValuteInfoDemo{}
	err := d.DecodeElement(&valuteDemo, &start)
	if err != nil {
		return err
	}

	valute.NumCode = valuteDemo.NumCode
	valute.CharCode = valuteDemo.CharCode

	valuteDemo.Value = strings.Replace(valuteDemo.Value, ",", ".", 1)
	valute.Value, err = strconv.ParseFloat(valuteDemo.Value, 64)
	if err != nil {
		return err
	}

	return nil
}

func main() {
	var ConfigPath Config
	wordPtr := flag.String("config", "yaml_src\\config.yaml", "yaml file")

	flag.Parse()
	yamlData, err := os.ReadFile(*wordPtr)
	if err != nil {
		fmt.Printf("Incorrect file name: %s\n", *wordPtr)
		panic(err)
	}

	err = yaml.Unmarshal(yamlData, &ConfigPath)
	if err != nil {
		fmt.Printf("Incorrect yaml file: %s\n", *wordPtr)
		panic(err)
	}

	xmlFile, err := os.Open(ConfigPath.Input)
	if err != nil {
		fmt.Printf("Incorrect path to input file: %s\n", ConfigPath.Input)
		panic(err)
	}
	defer xmlFile.Close()

	var valutes ValCurs

	xmlDec := xml.NewDecoder(xmlFile)

	xmlDec.CharsetReader = func(charset string, input io.Reader) (io.Reader, error) {
		switch charset {
		case "windows-1251":
			return charmap.Windows1251.NewDecoder().Reader(input), nil
		default:
			return nil, fmt.Errorf("unknown charset: %s", charset)
		}
	}
	err = xmlDec.Decode(&valutes)
	if err != nil {
		fmt.Printf("Unable to decode file: %s\n", ConfigPath.Input)
		panic(err)
	}

	sort.Slice(valutes.Valute[:], func(i, j int) bool {
		return valutes.Valute[i].Value > valutes.Valute[j].Value
	})

	jsonFile, err := json.MarshalIndent(valutes.Valute, "", " ")
	if err != nil {
		fmt.Println("Converting to json error")
		panic(err)
	}

	dir := filepath.Dir(ConfigPath.Output)
	err = os.MkdirAll(dir, 0777)
	if err != nil && !os.IsExist(err) {
		fmt.Printf("Unable to create path %s\n", dir)
		panic(err)
	}

	file, err := os.Create(ConfigPath.Output)
	if err != nil && !os.IsExist(err) {
		fmt.Printf("Unable to create file %s\n", ConfigPath.Output)
		panic(err)
	}
	file.Close()

	err = os.WriteFile(ConfigPath.Output, jsonFile, 0777)
	if err != nil {
		fmt.Printf("Unable to write to json file: %s", ConfigPath.Output)
		panic(err)
	}
}
