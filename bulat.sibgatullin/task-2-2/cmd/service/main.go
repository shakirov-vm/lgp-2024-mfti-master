package main

import (
	"container/heap"
	"fmt"
)

// An IntHeap is a max-heap of ints.
type IntHeap []int

func (h IntHeap) Len() int           { return len(h) }
func (h IntHeap) Less(i, j int) bool { return h[i] > h[j] } // In this task we need to find k-th value starting from the max
func (h IntHeap) Swap(i, j int)      { h[i], h[j] = h[j], h[i] }

func (h *IntHeap) Push(x any) {
	// Push and Pop use pointer receivers because they modify the slice's length,
	// not just its contents.
	*h = append(*h, x.(int))
}

func (h *IntHeap) Pop() any {
	old := *h
	n := len(old)
	x := old[n-1]
	*h = old[0 : n-1]
	return x
}

func workWithHeap(orderArr []int, k int) {
	h := &IntHeap{}
	heap.Init(h)

	for _, elem := range orderArr {
		heap.Push(h, elem)
	}

	for i := 0; i < k-1; i++ {
		heap.Pop(h)
	}

	fmt.Printf("%d\n", (*h)[0])
}
func main() {
	var N, k int

	_, err := fmt.Scanf("%d\n", &N)
	if err != nil {
		fmt.Printf("Error: %s", err)
		return
	}

	dishList := make([]int, N)

	for i := 0; i < N-1; i++ {
		_, err = fmt.Scan(&dishList[i])
		if err != nil {
			fmt.Printf("Error: %s", err)
			return
		}
	}

	_, err = fmt.Scanf("%d\n", &dishList[N-1])
	if err != nil {
		fmt.Printf("Error: %s", err)
		return
	}

	_, err = fmt.Scanf("%d\n", &k)
	if err != nil {
		fmt.Printf("Error: %s", err)
		return
	}

	workWithHeap(dishList, k)
}
