package main

import (
	"fmt"
)

type tempRange struct {
	min int
	max int
}

func main() {
	var N, K, temp int
	var compSign string

	_, err := fmt.Scanln(&N)
	if err != nil {
		panic(err)
	}
	departTemp := make([]tempRange, N)

	for i := 0; i < N; i++ {
		departTemp[i].min = 15
		departTemp[i].max = 30

		_, err = fmt.Scanln(&K)
		if err != nil {
			panic(err)
		}

		for j := 0; j < K; j++ {
			_, err = fmt.Scan(&compSign)
			if err != nil {
				panic(err)
			}

			_, err = fmt.Scan(&temp)
			if err != nil {
				panic(err)
			}

			if compSign == ">=" {
				if departTemp[i].min < temp {
					departTemp[i].min = temp
				}
			} else if compSign == "<=" {
				if departTemp[i].max > temp {
					departTemp[i].max = temp
				}
			} else {
				panic("Некорректный формат ввода!")
			}

			if departTemp[i].min <= departTemp[i].max && departTemp[i].min >= 15 && departTemp[i].max <= 30 {
				temp = departTemp[i].min
			} else {
				temp = -1
			}
			fmt.Println(temp)
		}
		fmt.Println()
	}
}
