package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	reader := bufio.NewReader(os.Stdin)

	for {
		fmt.Println("Введите первое число:")
		num1Str, _ := reader.ReadString('\n')
		num1Str = num1Str[:len(num1Str)-1]

		num1, err := strconv.ParseFloat(num1Str, 64)
		if err != nil {
			fmt.Println("Некорректное число. Пожалуйста, введите числовое значение.")
			continue
		}

		fmt.Println("Выберите операцию (+, -, *, /):")
		operator, _ := reader.ReadString('\n')
		operator = operator[:len(operator)-1]

		fmt.Println("Введите второе число:")
		num2Str, _ := reader.ReadString('\n')
		num2Str = num2Str[:len(num2Str)-1]

		num2, err := strconv.ParseFloat(num2Str, 64)
		if err != nil {
			fmt.Println("Некорректное число. Пожалуйста, введите числовое значение.")
			continue
		}

		result := 0.0
		switch operator {
		case "+":
			result = num1 + num2
		case "-":
			result = num1 - num2
		case "*":
			result = num1 * num2
		case "/":
			if num2 == 0 {
				fmt.Println("Ошибка: деление на ноль невозможно.")
				continue
			}
			result = num1 / num2
		default:
			fmt.Println("Некорректная операция. Пожалуйста, используйте символы +, -, * или /.")
			continue
		}

		fmt.Printf("Результат: %v %s %v = %v\n", num1, operator, num2, result)
	}
}
