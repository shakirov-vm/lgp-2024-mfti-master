### Project build: basic calculator

#### AST
To dump **AST** for the project (with dependencies):

``` bash
go build -gcflags=-w cmd/service/main.go &> AST/main.ast
```
*NO: use &> to redirect stream*

for a single file you can use:
``` bash
go tool compile -W main.go
```

#### SSA
To dump **SSA** for the project:
``` bash
GOSSAFUNC=main go build -a cmd/service/main.go > ssa.html
```
*NO: flag __-a__ is used to force rebuilding of packages that are already up-to-date*
- problem: how to direct stream to dir/ssa.html ?

#### Object file
``` bash
go build -gcflags=-S cmd/service/main.go &> obj/main.o
```
*NO: use &> to redirect stream*


#### Execute file
``` bash
go build cmd/service/main.go -o exe/main 
```

#### Build -tags directive

*NO: change __tag_name__ according to your name* 

in file:
``` bash
// go:build tag_name 
// +build tag_name # - to include to the final binary file
```
To build with tags:
``` bash
go build -tags tag_name
```
in my case:
``` bash
go build -o build/main_tag -tags=welcome ./cmd/service
```

#### Generate directive
in file(example):
``` bash
//go:generate mockgen -destination=mock_foo.go -package=generate . Doer 
```
To use generate directive:
``` bash
go generate ./... 
```

#### Embedded directive

in file to include other embedded files:
``` bash
import "embed"
// go:embed file_name
```

To use embedded directive:
``` bash
cd embed
echo "hello go" > folder/single_file.txt
echo "123" > folder/file1.hash
echo "456" > folder/file2.hash
go run ./embed.go  
```
