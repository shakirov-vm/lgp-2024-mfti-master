package input

import (
    "fmt"
    "os"
    "log"
    "strconv"
    "bufio"
    "strings"
)

func InputData(str string) float64 {
    scanner := bufio.NewScanner(os.Stdin)
    for {
        fmt.Print(str)
        scanner.Scan()
        if err := scanner.Err(); err != nil {
            log.Fatal(err)
        }

        value, err := strconv.ParseFloat(strings.TrimSpace(scanner.Text()), 64)
        if err != nil {
            fmt.Fprintf(os.Stderr, "error: Invalid input number\n")
            continue
        }
        return value
    }
}

func InputOperator(str string) string {
    scanner := bufio.NewScanner(os.Stdin)
    for {
        fmt.Print(str)
        scanner.Scan()
        if err := scanner.Err(); err != nil {
            log.Fatal(err)
        }

        line := strings.TrimSpace(scanner.Text())
        if (line == "+") || (line == "-") || (line == "*") || (line == "/") {
            return line
        } else {
            fmt.Fprintf(os.Stderr, "error: Invalid operator, try again\n")
        }
    }
}