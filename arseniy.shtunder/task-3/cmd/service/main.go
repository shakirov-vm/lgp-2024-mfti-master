package main

import (
	"fmt"
	"io"
	"os"
	"flag"
	"encoding/xml"
	"encoding/json"
	"strconv"
	"strings"
	"sort"
	"path/filepath"
	"gopkg.in/yaml.v3"
	"golang.org/x/text/encoding/charmap"
)

var configPath = "../../configs/config.yaml"
var configFile = flag.String("config", configPath, "path to config file")

type Config struct {
	Input  string `yaml:"input-file"`
	Output string `yaml:"output-file"`
}

type DataRawCBR struct {
	NumCode  int64    `xml:"NumCode"`
	CharCode string   `xml:"CharCode"`
	Value	 string   `xml:"Value"`
}

type DataCBR struct {
	NumCode  int64    `xml:"NumCode" json:"num_code"`
	CharCode string   `xml:"CharCode" json:"char_code"`
	Value	 float64  `xml:"Value" json:"value"`
}

type currencyInfo struct {
	Valute []DataCBR `xml:"Valute"`
 }


type ByValue []DataCBR

func (a ByValue) Len() int           { return len(a) }
func (a ByValue) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByValue) Less(i, j int) bool { return a[i].Value > a[j].Value }

func (currency *DataCBR) UnmarshalXML(decoder *xml.Decoder, start xml.StartElement) error {
	data := DataRawCBR{}
	err := decoder.DecodeElement(&data, &start)
	if err != nil {
		return err
	}

	currency.NumCode = data.NumCode
	currency.CharCode = data.CharCode
	currency.Value, err = strconv.ParseFloat(strings.Replace(data.Value, ",", ".", 1), 64)
	if err != nil {
		return err
	}

	return nil
}

func main() {
	var configData Config

	flag.Parse()
	
	// config file
	yamlFile, err := os.OpenFile(*configFile, os.O_RDONLY, 0666)
	if err != nil {
		fmt.Printf("Error to open config file: %s\n", *configFile)
		panic(err)
	}
	defer yamlFile.Close()

	// read config file 
	readConfig, err := io.ReadAll(yamlFile)
	if err != nil {
		fmt.Printf("Error to read config file: %s\n", *configFile)		
		panic(err)
	}

	// decoding the data to configData
	err = yaml.Unmarshal(readConfig, &configData)
	if err != nil {
		fmt.Printf("Incorrect config file: %s\n", *configFile)		
		panic(err)
	}

	// input file (read-only mode)
	inputFile, err := os.Open(configData.Input)
	if err != nil {
		fmt.Printf("Error to open input-file: %s\n", configData.Input)
		panic(err)
	}
	defer inputFile.Close()

	var currency currencyInfo
	decoder := xml.NewDecoder(inputFile)

	decoder.CharsetReader = func(charset string, input io.Reader) (io.Reader, error) {
		switch charset {
		case "windows-1251":
			return charmap.Windows1251.NewDecoder().Reader(input), nil
		default:
			return nil, fmt.Errorf("unknown charset: %s", charset)
		}
	}

	// decoding the data
	if err := decoder.Decode(&currency); err != nil {
		fmt.Printf("Error to decode data from input-file: %s\n", configData.Input)
		panic(err)
	}

	// sorting by value
	sort.Sort(ByValue(currency.Valute))
	
	// encoding the data
	jsonFile, err := json.MarshalIndent(currency.Valute, "", "\t")
	if err != nil {
		fmt.Println("Error to encode to json data")
		panic(err)
	}

	// check the existence dir
	err = os.MkdirAll(filepath.Dir(configData.Output), 0777) 
	if err != nil {
		fmt.Printf("Error to Mkdir for file: %s\n", configData.Output)
		panic(err)
	}

	// output file
	outputFile, err := os.OpenFile(configData.Output, os.O_RDWR | os.O_CREATE, 0666)
	if err != nil { 
		fmt.Printf("Error to open output file: %s\n", configData.Output)
		panic(err)
	}
	defer outputFile.Close()

	// writing to the file
	if _, err := outputFile.Write(jsonFile); err != nil {
		fmt.Printf("Error to write  ")
		panic(err)
	}
}
