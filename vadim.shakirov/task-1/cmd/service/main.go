package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {

	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Input format: \"<num32> <op> <num32>\". Available operations: +, -, *, /")
	for true {

		expression, err := reader.ReadString('\n')
		if err != nil {
			fmt.Println("Error in expression, enter expression again")
			continue
		}
		expression = strings.ReplaceAll(expression, "\n", "")

		tokens := strings.Split(expression, " ")
		if len(tokens) != 3 {
			fmt.Println("Invalid num of tokens, enter expression again")
			continue
		}

		// First token must be a number
		left, err := strconv.ParseInt(tokens[0], 10, 32)
		if err != nil {
			fmt.Println("Error in left operand, enter expression again")
			continue
		}

		// Second token is operation
		operation := tokens[1]

		// Third token must be a number
		right, err := strconv.ParseInt(tokens[2], 10, 32)
		if err != nil {
			fmt.Println("Error in right operand, enter expression again")
			continue
		}

		if operation == "+" {
			fmt.Println(left + right)
		} else if operation == "-" {
			fmt.Println(left - right)
		} else if operation == "*" {
			fmt.Println(left * right)
		} else if operation == "/" {
			fmt.Println(float64(left) / float64(right))
		} else {
			fmt.Println("Invalid operation")
		}
	}
}
