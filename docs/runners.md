# Как добавить собственный runner

## Setup local dind runner for GitLab
Для создания локального раннера в Docker (Docker in Docker) - необходимо:

1. Перейти в раздел `Settings -> CI/CD -> Runners` и выбрать `New project runner`.
> Важно - необходимо сохранить токен, который предоставит GitLab, после закрытия окна получить его повторно будет невозможно.
2. Запустить локальный Docker Runner на хост машине:
```bash
    docker volume create gitlab-runner-config
    docker run -d --name gitlab-runner --restart always \                             
        -v /var/run/docker.sock:/var/run/docker.sock \
        -v gitlab-runner-config:/etc/gitlab-runner \
        gitlab/gitlab-runner:latest
```
3. Запустить регистрацию локального раннера:
```bash
    docker exec -it gitlab-runner gitlab-runner register  
```
4. Optional: В конфигурации раннера (`gitlab-runner-config/config.toml`) изменить конфигурацию раннера
```config.toml
  [runners.docker]
    tls_verify = false
    image = "docker:stable"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache", "/var/run/docker.sock:/var/run/docker.sock"]
    shm_size = 0
    network_mtu = 0
```
5. Optional (for MacOS only): Настроить `DOCKER_HOST` на `unix` сокет:
```bash
    export DOCKER_HOST=unix:///var/run/docker.sock
``` 
